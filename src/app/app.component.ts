import { ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import * as L from "leaflet";
import "../../node_modules/leaflet-spin/leaflet.spin.min";

import { geoJSON, latLng, Map, Popup, tileLayer } from "leaflet";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import * as data from "../assets/data.json";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  @ViewChild("auto") auto: any;
  popup: Popup = new Popup();
  polygon: any;
  cities: any;
  items: any;
  dataRegion: any;
  data: any = (data as any).default;
  // Define our base layers so we can reference them multiple times
  streetMaps = tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    detectRetina: true,
    attribution: "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
  });
  // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  options = {
    layers: [this.streetMaps],
    zoom: 10,
    center: latLng([44.4471372, 8.7507472, 10]),
    minNativeZoom: 10,
    minZoom: 10

  };
  selectedCity: any;
  selectedItem: any;
  selectedRegion = false;
  isImgLoaded: boolean = false;
  mapIsLoaded = false;
  private unsubscribe$ = new Subject<void>();
  private geojson: any;
  private divMap: any;
  selectedCountryCode = 'it';
  countryCodes = ['it','gb'];
  showLabels = true;
  customLabels = {
    'gb': 'Inglese',
    'it': 'Italiano'
  };

  constructor(
    private httpClient: HttpClient,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang('it')
  }

  ngOnInit() {}

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
    this.translate.use(value);

  }

  clickRegion(feature: any) {
    if (feature.properties.items.length === 1) {
      window.location.href = feature.properties.items[0].link;
    } else if (feature.geometry.disable) {
      this.selectedRegion = true;
      this.selectedItem = false;
      const d = feature.properties;
      d.disable = true;
      this.dataRegion = d;
      this.changeDetector.detectChanges();
    } else {
      this.selectedRegion = true;
      this.selectedItem = false;
      this.dataRegion = feature.properties;
      this.changeDetector.detectChanges();
    }
  }

  onMapReady(map: Map) {
    this.mapIsLoaded = true;
    this.divMap = map;
    this.initMap();
  }

  focus(e: any): void {
    e.stopPropagation();
    this.auto.focus();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  handleChange($event?: any) {
    if ($event && $event.items.length === 1 ) {
      window.location.href = $event.items[0].link;
    } else {
      this.selectedItem = $event || null;
    }

  }

  handleClear($event: any) {
    this.selectedItem = null;
    this.initMap();
  }

  initMap() {
    this.divMap.spin(true);
    this.polygon = [];
    this.data.cities.forEach((el: any) => {
      if (el.geoBounding) {
        this.polygon.push(el.geoBounding.geoJson.features[0]);
      }
    });

    this.geojson = geoJSON(this.polygon, {
      style: (feature) => ({
        weight: 2,
        opacity: 0.5,
        color: "#ffffff",
        fillOpacity: 0.8,
        fillColor: "#97132f"
      }),
      onEachFeature: (feature, layer) => {
        layer.on({
          mouseover: (e) => this.highlightFeature(e),
          mouseout: (e) => this.resetFeature(e),
          click: (e) => this.clickRegion(feature)
        });
      }
    });
    setTimeout(() => {
      this.geojson.addTo(this.divMap);
      this.divMap.fitBounds(this.geojson.getBounds());
      this.divMap.spin(false);
    }, 1000);


  }

  private highlightFeature(e: any) {
    const layer = e.target;

    layer.setStyle({
      weight: 2,
      opacity: 1.0,
      color: "#DFA612",
      fillOpacity: 1.0,
      fillColor: "#FAE042"
    });

    //open popup
    this.popup = L.popup()
      .setLatLng(e.latlng)
      .setContent("<h6 style=\"color: grey; margin-bottom: 0;\">" + layer.feature.properties.name + "</h6>")
      .openOn(e.target._map);
  }

  private resetFeature(e: any) {
    const layer = e.target;

    layer.setStyle({
      weight: 2,
      opacity: 0.5,
      color: "#ffffff",
      fillOpacity: 0.8,
      fillColor: "#97132f"
    });

    this.popup.onRemove(e.target._map);
  }
}
