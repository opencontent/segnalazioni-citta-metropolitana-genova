import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import {RouterModule} from "@angular/router";
import {ApiService} from "./services/api.service";
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from "@angular/common/http";
import {Interceptor} from "./interceptor/Interceptor";
import { NgSelectModule } from "@ng-select/ng-select";
import { MatButtonModule } from "@angular/material/button";
import { CommonModule } from "@angular/common";
import { NgxFlagPickerModule } from "ngx-flag-picker";
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LeafletModule,
    RouterModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    MatButtonModule,
    NgxFlagPickerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    ApiService
  ],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
