import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  getData(){
   return this.http.get<any>(environment.cmgApiUrl + '/api/sensor/areas?limit=100&cursor=%2A')
  }

}
