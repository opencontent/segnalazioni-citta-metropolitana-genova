# Stage 1: Compile and Build angular codebase
ARG BASE_URL=http://app.opencontent.it

# Use official node image as the base image
FROM node:18-slim as build

# Set the working directory
WORKDIR /usr/local/app

# Add the source code to app
COPY ./ /usr/local/app/

## Install all the dependencies
RUN npm install -g @angular/cli
RUN npm install --legacy-peer-deps

## Generate the build of the application
RUN ng build --prod -- --base-href $BASE_URL


# Stage 2: Serve app with caddy server

# Use official caddy image as the base image
FROM caddy:2.6.2

COPY --from=build /usr/local/app/public/single-page-genova .

WORKDIR /srv

EXPOSE 80 443 2015

CMD ["caddy", "file-server", "--root", "/srv", "--browse"]
